import 'package:get/get.dart';

class AppStrings {
  ///presentation .login strings

  static String userName = "Username or Mobile Number".tr;
  static String password = "Password".tr;
  static String terminalKey = "Terminal Key".tr;
  static String login = "Login".tr;
  static String invalidInput = "Invalid Input".tr;
  static String invalidPassword = "Invalid Password".tr;
  static String invalidTerminalKey = "Invalid Terminal Key".tr;

  ///create customer
  ///
  static String customers = "Customers".tr;
  static String customerArabicName = "Customer Name (Arabic)".tr;
  static String customerEnName = "Customer Name (English)".tr;
  static String address = "Address".tr;
  static String city = "City".tr;
  static String phoneNumber = "Phone Number".tr;
  static String email = "Email".tr;
  static String commercialRegister = "Commercial Register".tr;
  static String taxNumber = "Tax Number".tr;
  static String firstStore = "First Store".tr;
  static String customerCategoryName = "Customer Category Name".tr;
  static String save = "Save".tr;
  static String back = "Back".tr;
  static String active = "Active".tr;

  static String invalidCustomerName = "invalid Name".tr;
  static String invalidAddress = "invalid Address".tr;
  static String invalidCity = "invalid City".tr;
  static String invalidPhoneNumber = "invalid Phone Number".tr;
  static String invalidEmail = "invalid Email".tr;
  static String invalidCommercialRegister = "invalid Commercial Register".tr;
  static String invalidTaxNumber = "invalid Tax Number".tr;
  static String invalidFirstStore = "invalid First Store".tr;
  static String invalidCustomerCategoryName =
      "invalid Customer Category Name".tr;

  // get customer
  static String searchByCustomerName = "search by customer name";
  static String category = "category";
  static String inactive = "inactive";
  static String deliveryBig = "DELIVERY";
  static String delivery = "Delivery";
  static String addCustomer = "ADD CUSTOMER";
  String screenIsEmpty = "لا يوجد بيانات حاليا".tr;

  /// pagination messages
  String pullUpToLoadMore = "اسحب للأعلى لتحميل المزيد".tr;
  String failClickToRefresh = "فشل في التحميل اضغط لإعادة المحاولة".tr;
  String releaseToLoadMore = "حرر لتحميل المزيد".tr;
  String noMoreData = "لايوجد بيانات أكثر".tr;
}
