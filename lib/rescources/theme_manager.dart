import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ytech/rescources/style_manager.dart';

import '../app/app_controller.dart';
import 'color_manager.dart';
import 'design_constants.dart';

MaterialColor primarySwatch = const MaterialColor(0xFF00A3E4, {
  50: Color(0xFF00A3E4),
  100: Color(0xFF00A3E4),
  200: Color(0xFF00A3E4),
  300: Color(0xFF00A3E4),
  400: Color(0xFF00A3E4),
  500: Color(0xFF00A3E4),
  600: Color(0xFF00A3E4),
  700: Color(0xFF00A3E4),
  800: Color(0xFF00A3E4),
  900: Color(0xFF00A3E4)
});

final AppController appController = Get.find<AppController>();

ThemeData getLightTheme() {
  return ThemeData(
      colorScheme: ColorScheme(
          brightness: Brightness.light,
          primary: ColorManager.primaryColor,
          onPrimary: ColorManager.yellow,
          secondary: ColorManager.pink,
          onSecondary: ColorManager.lightGrey,
          error: ColorManager.redError,
          onError: ColorManager.darkPink,
          background: ColorManager.grey,
          onBackground: ColorManager.darkGrey,
          surface: ColorManager.black1,
          onSurface: ColorManager.littleDarkBlueBlack),
      brightness: Brightness.light,
      primaryColor: ColorManager.primaryColor,
      secondaryHeaderColor: ColorManager.pink,
      primaryColorLight: ColorManager.green,
      scaffoldBackgroundColor: Colors.white,
      shadowColor: ColorManager.lightGrey,
      highlightColor: ColorManager.grey,
      errorColor: ColorManager.redError,
      textTheme: TextTheme(
        // displayLarge:
        //     getBoldStyle(color: ColorManager.navyBlue, fontSize: 34.sp),
        displayMedium:
            getMediumStyle(color: ColorManager.white, fontSize: 12.sp),
        // displaySmall: getRegularItalicStyle(
        //     color: ColorManager.darkGrey, fontSize: 12.sp),

        // headlineLarge:
        //     getSemiBoldStyle(color: ColorManager.darkGrey, fontSize: 12.sp),
        // headlineMedium:
        //     getMediumStyle(color: ColorManager.darkGrey, fontSize: 15.sp),
        // headlineSmall: getRegularItalicStyle(
        //     color: ColorManager.darkGrey, fontSize: 12.sp),
        //
        titleLarge: getBoldStyle(color: ColorManager.black1, fontSize: 42.sp),
        // titleMedium:
        //     getBoldStyle(color: ColorManager.darkGrey, fontSize: 16.sp),
        // titleSmall:
        //     getRegularStyle(color: ColorManager.darkBlack, fontSize: 14.sp),
        //
        // bodyLarge: getBoldStyle(
        //     color: ColorManager.littleDarkBlueBlack, fontSize: 32.sp),
        bodyMedium: getMediumStyle(
            color: ColorManager.grey, fontSize: appController.width * 0.012),
        bodySmall:
            getRegularStyle(color: ColorManager.lightGrey, fontSize: 13.sp),

        // labelLarge: TextStyle(
        //   color: ColorManager.littleDarkGrey,
        //   fontSize: appController.width * 0.01,
        // ),
        labelMedium:
            getMediumStyle(color: ColorManager.green1, fontSize: 20.sp),
        labelSmall: getRegularStyle(
          color: ColorManager.darkGrey.withOpacity(0.62),
          fontSize: 13.sp,
        ),
      ),
      hintColor: ColorManager.darkGrey.withOpacity(0.32),
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
        foregroundColor: Colors.white,
        backgroundColor: ColorManager.pink,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(100.r)),
        // elevation: 6,
        minimumSize: Size(24.w, 62.h),
        maximumSize: Size(400.w, 80.h),
        // padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 16.h),
        textStyle: getRegularStyle(color: ColorManager.white, fontSize: 14.sp),
      )),
      // iconTheme: IconThemeData(
      //   color: Colors.black,
      //   size: 24.w,
      //   opacity: 1,
      // ),
      // appBarTheme: AppBarTheme(
      //   centerTitle: true,
      //   color: ColorManager.black,
      //   toolbarHeight: 75.h,
      //   titleTextStyle: TextStyle(
      //       fontFamily: "Hind",
      //       fontSize: 22.sp,
      //       fontWeight: FontWeight.w500,
      //       height: null,
      //       color: Colors.white),
      //   iconTheme: IconThemeData(
      //     color: ColorManager.white,
      //     size: 24.w,
      //     opacity: 1,
      //   ),
      // ),
      dividerTheme: DividerThemeData(
          color: ColorManager.littleDarkBlueBlack.withOpacity(0.64),
          thickness: 0.5,
          space: 1),
      listTileTheme: ListTileThemeData(
        contentPadding: EdgeInsets.symmetric(horizontal: 4.w),
      ),
      // bottomNavigationBarTheme: BottomNavigationBarThemeData(
      //   backgroundColor: Colors.white,
      //   unselectedIconTheme:
      //       const IconThemeData(color: Colors.black45, size: 20),
      //   unselectedItemColor: Colors.black45,
      //   unselectedLabelStyle: TextStyle(
      //       fontSize: 13,
      //       fontWeight: FontWeight.w700,
      //       color: ColorManager.black),
      //   selectedIconTheme: IconThemeData(color: ColorManager.black, size: 25),
      //   selectedItemColor: ColorManager.black,
      //   selectedLabelStyle: TextStyle(
      //       fontSize: 13,
      //       fontWeight: FontWeight.w700,
      //       color: ColorManager.black),
      //   elevation: 20.0,
      // ),

      cardTheme: CardTheme(
          elevation: 8,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
            //set border radius more than 50% of height and width to make circle
          )),
      inputDecorationTheme: InputDecorationTheme(
        hintStyle: Get.theme.textTheme.bodySmall?.copyWith(fontSize: 14.sp),
        border: kOutlinedBorderGray,
        focusedBorder: kOutlinedBorderLightGrey,
        enabledBorder: kOutlinedBorderGray,
        errorBorder: kOutlinedBorderRed,
        contentPadding: EdgeInsets.symmetric(horizontal: 12.w),
      ));
}
