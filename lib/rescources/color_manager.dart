import 'package:flutter/material.dart';

class ColorManager {
  static Color primaryColor = const Color(0xFF049cf4);
  static Color lightWhiteGrey = const Color(0xFFebecea);
  static Color yellow = const Color(0xFFebba11);

  static Color white = const Color(0xFFfbfbfb);

  static Color lightGrey = const Color(0xFFa7a4a0);
  static Color grey = const Color(0xFF808d88);
  static Color darkGrey = const Color(0xFF3c3c42);

  static Color black1 = const Color(0xFF1c1c2c);
  static Color littleDarkBlueBlack = const Color(0xFF343444);

  static Color pink = const Color(0xfff1495f);
  static Color pink1 = const Color(0xFFcd6c85);
  static Color darkPink = const Color(0xFF7a4049);
  static Color pink2 = const Color(0xFFcea390);
  static Color pink3 = const Color(0xFFf5aebf);
  static Color pink4 = const Color(0xFFe3cdcc);

  static Color green = const Color(0xFF3bab5e);
  static Color green1 = const Color(0xFF6dbc91);
  static Color green2 = const Color(0xFFaff9d3);

  static Color redError = const Color(0xFF831A26);
}
