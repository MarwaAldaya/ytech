const String IMAGE_PATH = "assets/images";
const String JSON_PATH = "assets/json";
const String ICON_PATH = "assets/icon";

class ImageAssets {
  static const String login = "$IMAGE_PATH/login.png";
  static const String card = "$IMAGE_PATH/card.png";
  static const String person = "$IMAGE_PATH/person.jpg";
  static const String noData = "$IMAGE_PATH/no_data.png";
}

class JsonAssets {}
