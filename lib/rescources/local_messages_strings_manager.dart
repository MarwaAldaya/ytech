import 'package:get/get.dart';

class AppLocalMessagesStringsManager {
  // error handler
  String success = "Success".tr;
  String badRequestError = "bad request error".tr;
  String noContent = "no content".tr;
  String forbiddenError = "forbidden error".tr;
  String unauthorizedError = "unauthorized error".tr;
  String notFoundError = "not found error".tr;
  String conflictError = "conflict error".tr;
  String internalServerError = "internal server error".tr;
  String unknownError = "unknown error".tr;
  String timeoutError = "timeout error".tr;
  String defaultError = "some thing went wrong".tr;
  String cacheError = "cache error".tr;
  String noInternetError = "no internet error".tr;

// messages
  String requiredFieldsMustNotBeEmpty = "Required Fields Must Not Be Empty".tr;
}
