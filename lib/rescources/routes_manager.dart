
import 'package:get/get.dart';
import 'package:ytech/presentation/create_customer/create_customer_binding.dart';
import 'package:ytech/presentation/create_customer/create_customer_view.dart';
import 'package:ytech/presentation/login/login_binding.dart';


import '../presentation/customer/customers_binding.dart';
import '../presentation/customer/customers_view.dart';
import '../presentation/login/login_view.dart';

class Routes {
  static const String splashRoute = "/";

  static const String loginRoute = "/presentation.presentation.login";
  static const String customersRoute = "/customers";
  static const String createCustomerRoute = "/create_customer";

}

class AppPages {
  AppPages._();


  static final List<GetPage> pages = [

    GetPage(
        name: Routes.loginRoute,
        page: () => const LoginView(),
        binding: LoginBinding()),
    GetPage(
        name: Routes.customersRoute,
        page: () => const CustomersView(),
        binding: CustomersBinding()),

    GetPage(
        name: Routes.createCustomerRoute,
        page: () => const CreateCustomerView(),
        binding: CreateCustomerBinding()),

  ];
}
