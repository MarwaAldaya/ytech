import 'package:json_annotation/json_annotation.dart';

part 'login_params.g.dart';

@JsonSerializable(createToJson: true)
class LoginParams {
  String username;
  String password;
  String? terminlKey;

  LoginParams(
      {required this.username, required this.password, this.terminlKey});

  Map<String, dynamic> toJson() => {
        "username": username,
        "password": password,
        "terminlKey": "GNrHw7jLaV5AFdi20m5HI02fVTn+hCBkJPr7UL7ThSw="
      };
}
