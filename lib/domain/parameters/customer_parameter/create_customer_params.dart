import 'package:json_annotation/json_annotation.dart';

part 'create_customer_params.g.dart';

@JsonSerializable()
class CreateCustomerParams {
  final String id;
  @JsonKey(name: 'enFullName')
  final String fullNameEn;
  @JsonKey(name: 'arFullName')
  final String fullNameAr;
  final String? address;
  final String? email;
  final String? commercialRegister;
  final String phoneNumber;
  final String? taxIdNumber;
  final bool isDelivery;
  final String? customerCategoryId;
  final bool isActive;

  CreateCustomerParams({
    required this.id,
    required this.fullNameEn,
    required this.fullNameAr,
    this.address,
    this.email,
    this.commercialRegister,
    required this.phoneNumber,
    this.taxIdNumber,
    required this.isDelivery,
    this.customerCategoryId,
    required this.isActive,
  });

  Map<String, dynamic> toJson() => _$CreateCustomerParamsToJson(this);
}
