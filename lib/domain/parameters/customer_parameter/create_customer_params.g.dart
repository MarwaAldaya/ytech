// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_customer_params.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateCustomerParams _$CreateCustomerParamsFromJson(
        Map<String, dynamic> json) =>
    CreateCustomerParams(
      id: json['id'] as String,
      fullNameEn: json['enFullName'] as String,
      fullNameAr: json['arFullName'] as String,
      address: json['address'] as String?,
      email: json['email'] as String?,
      commercialRegister: json['commercialRegister'] as String?,
      phoneNumber: json['phoneNumber'] as String,
      taxIdNumber: json['taxIdNumber'] as String?,
      isDelivery: json['isDelivery'] as bool,
      customerCategoryId: json['customerCategoryId'] as String?,
      isActive: json['isActive'] as bool,
    );

Map<String, dynamic> _$CreateCustomerParamsToJson(
        CreateCustomerParams instance) =>
    <String, dynamic>{
      'id': instance.id,
      'enFullName': instance.fullNameEn,
      'arFullName': instance.fullNameAr,
      'address': instance.address,
      'email': instance.email,
      'commercialRegister': instance.commercialRegister,
      'phoneNumber': instance.phoneNumber,
      'taxIdNumber': instance.taxIdNumber,
      'isDelivery': instance.isDelivery,
      'customerCategoryId': instance.customerCategoryId,
      'isActive': instance.isActive,
    };
