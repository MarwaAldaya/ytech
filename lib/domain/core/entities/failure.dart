// ignore_for_file: non_constant_identifier_names

class Failure {
  int? code; // 200, 201, 400, 303..500 and so on
  String? message; // error , success

  Failure(this.code, this.message);

  Failure.Default();
}
