// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pagination_params.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaginationParams _$PaginationParamsFromJson(Map<String, dynamic> json) =>
    PaginationParams(
      pageNumber: json['ParameterPagination.PageNumber'] as int,
      pageSize: json['ParameterPagination.PageSize'] as int?,
    );

Map<String, dynamic> _$PaginationParamsToJson(PaginationParams instance) =>
    <String, dynamic>{
      'ParameterPagination.PageNumber': instance.pageNumber,
      'ParameterPagination.PageSize': instance.pageSize,
    };
