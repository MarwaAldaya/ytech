import 'package:json_annotation/json_annotation.dart';

part 'pagination_params.g.dart';

@JsonSerializable(createToJson: true)
class PaginationParams {
  @JsonKey(name: 'ParameterPagination.PageNumber')
  final int pageNumber;
  @JsonKey(name: 'ParameterPagination.PageSize')
  final int? pageSize;

  PaginationParams({required this.pageNumber, this.pageSize});

   Map<String, dynamic> toJson() => _$PaginationParamsToJson(this);
}
