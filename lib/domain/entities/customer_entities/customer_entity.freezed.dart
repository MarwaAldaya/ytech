// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'customer_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CustomerEntity {
  String get id => throw _privateConstructorUsedError;
  String get fullNameEn => throw _privateConstructorUsedError;
  String get fullNameAr => throw _privateConstructorUsedError;
  String get displayName => throw _privateConstructorUsedError;
  String? get address => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get commercialRegister => throw _privateConstructorUsedError;
  String get phoneNumber => throw _privateConstructorUsedError;
  String? get city => throw _privateConstructorUsedError;
  String? get store => throw _privateConstructorUsedError;
  String? get taxIdNumber => throw _privateConstructorUsedError;
  bool? get loyalty => throw _privateConstructorUsedError;
  bool? get isDelivery => throw _privateConstructorUsedError;
  double? get commission => throw _privateConstructorUsedError;
  double? get loyaltyBalance => throw _privateConstructorUsedError;
  bool? get isActive => throw _privateConstructorUsedError;
  bool? get isDefault => throw _privateConstructorUsedError;
  String? get customerCategoryId => throw _privateConstructorUsedError;
  double? get creditLimit => throw _privateConstructorUsedError;
  String? get financialAccount => throw _privateConstructorUsedError;
  DateTime get creationTime => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CustomerEntityCopyWith<CustomerEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerEntityCopyWith<$Res> {
  factory $CustomerEntityCopyWith(
          CustomerEntity value, $Res Function(CustomerEntity) then) =
      _$CustomerEntityCopyWithImpl<$Res, CustomerEntity>;
  @useResult
  $Res call(
      {String id,
      String fullNameEn,
      String fullNameAr,
      String displayName,
      String? address,
      String? email,
      String? commercialRegister,
      String phoneNumber,
      String? city,
      String? store,
      String? taxIdNumber,
      bool? loyalty,
      bool? isDelivery,
      double? commission,
      double? loyaltyBalance,
      bool? isActive,
      bool? isDefault,
      String? customerCategoryId,
      double? creditLimit,
      String? financialAccount,
      DateTime creationTime});
}

/// @nodoc
class _$CustomerEntityCopyWithImpl<$Res, $Val extends CustomerEntity>
    implements $CustomerEntityCopyWith<$Res> {
  _$CustomerEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? fullNameEn = null,
    Object? fullNameAr = null,
    Object? displayName = null,
    Object? address = freezed,
    Object? email = freezed,
    Object? commercialRegister = freezed,
    Object? phoneNumber = null,
    Object? city = freezed,
    Object? store = freezed,
    Object? taxIdNumber = freezed,
    Object? loyalty = freezed,
    Object? isDelivery = freezed,
    Object? commission = freezed,
    Object? loyaltyBalance = freezed,
    Object? isActive = freezed,
    Object? isDefault = freezed,
    Object? customerCategoryId = freezed,
    Object? creditLimit = freezed,
    Object? financialAccount = freezed,
    Object? creationTime = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      fullNameEn: null == fullNameEn
          ? _value.fullNameEn
          : fullNameEn // ignore: cast_nullable_to_non_nullable
              as String,
      fullNameAr: null == fullNameAr
          ? _value.fullNameAr
          : fullNameAr // ignore: cast_nullable_to_non_nullable
              as String,
      displayName: null == displayName
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      commercialRegister: freezed == commercialRegister
          ? _value.commercialRegister
          : commercialRegister // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      city: freezed == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      store: freezed == store
          ? _value.store
          : store // ignore: cast_nullable_to_non_nullable
              as String?,
      taxIdNumber: freezed == taxIdNumber
          ? _value.taxIdNumber
          : taxIdNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      loyalty: freezed == loyalty
          ? _value.loyalty
          : loyalty // ignore: cast_nullable_to_non_nullable
              as bool?,
      isDelivery: freezed == isDelivery
          ? _value.isDelivery
          : isDelivery // ignore: cast_nullable_to_non_nullable
              as bool?,
      commission: freezed == commission
          ? _value.commission
          : commission // ignore: cast_nullable_to_non_nullable
              as double?,
      loyaltyBalance: freezed == loyaltyBalance
          ? _value.loyaltyBalance
          : loyaltyBalance // ignore: cast_nullable_to_non_nullable
              as double?,
      isActive: freezed == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool?,
      isDefault: freezed == isDefault
          ? _value.isDefault
          : isDefault // ignore: cast_nullable_to_non_nullable
              as bool?,
      customerCategoryId: freezed == customerCategoryId
          ? _value.customerCategoryId
          : customerCategoryId // ignore: cast_nullable_to_non_nullable
              as String?,
      creditLimit: freezed == creditLimit
          ? _value.creditLimit
          : creditLimit // ignore: cast_nullable_to_non_nullable
              as double?,
      financialAccount: freezed == financialAccount
          ? _value.financialAccount
          : financialAccount // ignore: cast_nullable_to_non_nullable
              as String?,
      creationTime: null == creationTime
          ? _value.creationTime
          : creationTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CustomerEntityCopyWith<$Res>
    implements $CustomerEntityCopyWith<$Res> {
  factory _$$_CustomerEntityCopyWith(
          _$_CustomerEntity value, $Res Function(_$_CustomerEntity) then) =
      __$$_CustomerEntityCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String fullNameEn,
      String fullNameAr,
      String displayName,
      String? address,
      String? email,
      String? commercialRegister,
      String phoneNumber,
      String? city,
      String? store,
      String? taxIdNumber,
      bool? loyalty,
      bool? isDelivery,
      double? commission,
      double? loyaltyBalance,
      bool? isActive,
      bool? isDefault,
      String? customerCategoryId,
      double? creditLimit,
      String? financialAccount,
      DateTime creationTime});
}

/// @nodoc
class __$$_CustomerEntityCopyWithImpl<$Res>
    extends _$CustomerEntityCopyWithImpl<$Res, _$_CustomerEntity>
    implements _$$_CustomerEntityCopyWith<$Res> {
  __$$_CustomerEntityCopyWithImpl(
      _$_CustomerEntity _value, $Res Function(_$_CustomerEntity) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? fullNameEn = null,
    Object? fullNameAr = null,
    Object? displayName = null,
    Object? address = freezed,
    Object? email = freezed,
    Object? commercialRegister = freezed,
    Object? phoneNumber = null,
    Object? city = freezed,
    Object? store = freezed,
    Object? taxIdNumber = freezed,
    Object? loyalty = freezed,
    Object? isDelivery = freezed,
    Object? commission = freezed,
    Object? loyaltyBalance = freezed,
    Object? isActive = freezed,
    Object? isDefault = freezed,
    Object? customerCategoryId = freezed,
    Object? creditLimit = freezed,
    Object? financialAccount = freezed,
    Object? creationTime = null,
  }) {
    return _then(_$_CustomerEntity(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      fullNameEn: null == fullNameEn
          ? _value.fullNameEn
          : fullNameEn // ignore: cast_nullable_to_non_nullable
              as String,
      fullNameAr: null == fullNameAr
          ? _value.fullNameAr
          : fullNameAr // ignore: cast_nullable_to_non_nullable
              as String,
      displayName: null == displayName
          ? _value.displayName
          : displayName // ignore: cast_nullable_to_non_nullable
              as String,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      commercialRegister: freezed == commercialRegister
          ? _value.commercialRegister
          : commercialRegister // ignore: cast_nullable_to_non_nullable
              as String?,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      city: freezed == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      store: freezed == store
          ? _value.store
          : store // ignore: cast_nullable_to_non_nullable
              as String?,
      taxIdNumber: freezed == taxIdNumber
          ? _value.taxIdNumber
          : taxIdNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      loyalty: freezed == loyalty
          ? _value.loyalty
          : loyalty // ignore: cast_nullable_to_non_nullable
              as bool?,
      isDelivery: freezed == isDelivery
          ? _value.isDelivery
          : isDelivery // ignore: cast_nullable_to_non_nullable
              as bool?,
      commission: freezed == commission
          ? _value.commission
          : commission // ignore: cast_nullable_to_non_nullable
              as double?,
      loyaltyBalance: freezed == loyaltyBalance
          ? _value.loyaltyBalance
          : loyaltyBalance // ignore: cast_nullable_to_non_nullable
              as double?,
      isActive: freezed == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool?,
      isDefault: freezed == isDefault
          ? _value.isDefault
          : isDefault // ignore: cast_nullable_to_non_nullable
              as bool?,
      customerCategoryId: freezed == customerCategoryId
          ? _value.customerCategoryId
          : customerCategoryId // ignore: cast_nullable_to_non_nullable
              as String?,
      creditLimit: freezed == creditLimit
          ? _value.creditLimit
          : creditLimit // ignore: cast_nullable_to_non_nullable
              as double?,
      financialAccount: freezed == financialAccount
          ? _value.financialAccount
          : financialAccount // ignore: cast_nullable_to_non_nullable
              as String?,
      creationTime: null == creationTime
          ? _value.creationTime
          : creationTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$_CustomerEntity extends _CustomerEntity {
  const _$_CustomerEntity(
      {required this.id,
      required this.fullNameEn,
      required this.fullNameAr,
      required this.displayName,
      this.address,
      this.email,
      this.commercialRegister,
      required this.phoneNumber,
      this.city,
      this.store,
      this.taxIdNumber,
      this.loyalty,
      this.isDelivery,
      this.commission,
      this.loyaltyBalance,
      this.isActive,
      this.isDefault,
      this.customerCategoryId,
      this.creditLimit,
      this.financialAccount,
      required this.creationTime})
      : super._();

  @override
  final String id;
  @override
  final String fullNameEn;
  @override
  final String fullNameAr;
  @override
  final String displayName;
  @override
  final String? address;
  @override
  final String? email;
  @override
  final String? commercialRegister;
  @override
  final String phoneNumber;
  @override
  final String? city;
  @override
  final String? store;
  @override
  final String? taxIdNumber;
  @override
  final bool? loyalty;
  @override
  final bool? isDelivery;
  @override
  final double? commission;
  @override
  final double? loyaltyBalance;
  @override
  final bool? isActive;
  @override
  final bool? isDefault;
  @override
  final String? customerCategoryId;
  @override
  final double? creditLimit;
  @override
  final String? financialAccount;
  @override
  final DateTime creationTime;

  @override
  String toString() {
    return 'CustomerEntity(id: $id, fullNameEn: $fullNameEn, fullNameAr: $fullNameAr, displayName: $displayName, address: $address, email: $email, commercialRegister: $commercialRegister, phoneNumber: $phoneNumber, city: $city, store: $store, taxIdNumber: $taxIdNumber, loyalty: $loyalty, isDelivery: $isDelivery, commission: $commission, loyaltyBalance: $loyaltyBalance, isActive: $isActive, isDefault: $isDefault, customerCategoryId: $customerCategoryId, creditLimit: $creditLimit, financialAccount: $financialAccount, creationTime: $creationTime)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CustomerEntity &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.fullNameEn, fullNameEn) ||
                other.fullNameEn == fullNameEn) &&
            (identical(other.fullNameAr, fullNameAr) ||
                other.fullNameAr == fullNameAr) &&
            (identical(other.displayName, displayName) ||
                other.displayName == displayName) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.commercialRegister, commercialRegister) ||
                other.commercialRegister == commercialRegister) &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            (identical(other.city, city) || other.city == city) &&
            (identical(other.store, store) || other.store == store) &&
            (identical(other.taxIdNumber, taxIdNumber) ||
                other.taxIdNumber == taxIdNumber) &&
            (identical(other.loyalty, loyalty) || other.loyalty == loyalty) &&
            (identical(other.isDelivery, isDelivery) ||
                other.isDelivery == isDelivery) &&
            (identical(other.commission, commission) ||
                other.commission == commission) &&
            (identical(other.loyaltyBalance, loyaltyBalance) ||
                other.loyaltyBalance == loyaltyBalance) &&
            (identical(other.isActive, isActive) ||
                other.isActive == isActive) &&
            (identical(other.isDefault, isDefault) ||
                other.isDefault == isDefault) &&
            (identical(other.customerCategoryId, customerCategoryId) ||
                other.customerCategoryId == customerCategoryId) &&
            (identical(other.creditLimit, creditLimit) ||
                other.creditLimit == creditLimit) &&
            (identical(other.financialAccount, financialAccount) ||
                other.financialAccount == financialAccount) &&
            (identical(other.creationTime, creationTime) ||
                other.creationTime == creationTime));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        fullNameEn,
        fullNameAr,
        displayName,
        address,
        email,
        commercialRegister,
        phoneNumber,
        city,
        store,
        taxIdNumber,
        loyalty,
        isDelivery,
        commission,
        loyaltyBalance,
        isActive,
        isDefault,
        customerCategoryId,
        creditLimit,
        financialAccount,
        creationTime
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CustomerEntityCopyWith<_$_CustomerEntity> get copyWith =>
      __$$_CustomerEntityCopyWithImpl<_$_CustomerEntity>(this, _$identity);
}

abstract class _CustomerEntity extends CustomerEntity {
  const factory _CustomerEntity(
      {required final String id,
      required final String fullNameEn,
      required final String fullNameAr,
      required final String displayName,
      final String? address,
      final String? email,
      final String? commercialRegister,
      required final String phoneNumber,
      final String? city,
      final String? store,
      final String? taxIdNumber,
      final bool? loyalty,
      final bool? isDelivery,
      final double? commission,
      final double? loyaltyBalance,
      final bool? isActive,
      final bool? isDefault,
      final String? customerCategoryId,
      final double? creditLimit,
      final String? financialAccount,
      required final DateTime creationTime}) = _$_CustomerEntity;
  const _CustomerEntity._() : super._();

  @override
  String get id;
  @override
  String get fullNameEn;
  @override
  String get fullNameAr;
  @override
  String get displayName;
  @override
  String? get address;
  @override
  String? get email;
  @override
  String? get commercialRegister;
  @override
  String get phoneNumber;
  @override
  String? get city;
  @override
  String? get store;
  @override
  String? get taxIdNumber;
  @override
  bool? get loyalty;
  @override
  bool? get isDelivery;
  @override
  double? get commission;
  @override
  double? get loyaltyBalance;
  @override
  bool? get isActive;
  @override
  bool? get isDefault;
  @override
  String? get customerCategoryId;
  @override
  double? get creditLimit;
  @override
  String? get financialAccount;
  @override
  DateTime get creationTime;
  @override
  @JsonKey(ignore: true)
  _$$_CustomerEntityCopyWith<_$_CustomerEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
