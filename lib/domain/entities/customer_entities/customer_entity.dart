import 'package:freezed_annotation/freezed_annotation.dart';

part 'customer_entity.freezed.dart';

@freezed
class CustomerEntity with _$CustomerEntity {
  const factory CustomerEntity({
    required String id,
    required String fullNameEn,
    required String fullNameAr,
    required String displayName,
    String? address,
    String? email,
    String? commercialRegister,
    required String phoneNumber,
    String? city,
    String? store,
    String? taxIdNumber,
    bool? loyalty,
    bool? isDelivery,
    double? commission,
    double? loyaltyBalance,
    bool? isActive,
    bool? isDefault,
    String? customerCategoryId,
    double? creditLimit,
    String? financialAccount,
    required DateTime creationTime,
  }) = _CustomerEntity;

  const CustomerEntity._();
}
