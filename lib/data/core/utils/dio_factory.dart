import 'package:dio/dio.dart';
import 'package:dio_logger/dio_logger.dart';
import 'package:flutter/foundation.dart';
import 'package:ytech/app/dependency_injection.dart';

import '../../../app/app_prefrences.dart';
import '../../../app/constants.dart';
import 'api_routes/api_routes.dart';

const String APPLICATION_JSON = "application/json";
const String CONTENT_TYPE = "content-type";
const String ACCEPT = "accept";
const String ACCEPT_LANGUAGE = "Accept-Language";
const String AUTHORIZATION = "authorization";
const String TERMINAL_KEY = "Terminalkey";
const String APP_NAME = "App-Name";

class DioFactory {
  final AppPreferences _appPreferences;

  DioFactory(this._appPreferences);

  Dio dio = Dio();

  Future<Dio> getDio() async {
    Map<String, String> headers = {
      CONTENT_TYPE: APPLICATION_JSON,
      ACCEPT: APPLICATION_JSON,
      TERMINAL_KEY: "GNrHw7jLaV5AFdi20m5HI02fVTn+hCBkJPr7UL7ThSw=",
      APP_NAME: "Ytech-Pos",
      // AUTHORIZATION: token!,
    };

    dio.options = BaseOptions(
        baseUrl: AppUrls.baseUrl,
        headers: headers,
        receiveTimeout: Constants.apiTimeOut,
        sendTimeout: Constants.apiTimeOut);

    if (!kReleaseMode) {
      dio.interceptors.add(dioLoggerInterceptor);
    }

    return dio;
  }

  Future<void> injectTokenToHeader() async {
    String userToken = '';
    await instance<AppPreferences>()
        .getAccessToken()
        .then((value) => userToken = value);

    if (userToken.isNotEmpty) {
      dio.interceptors.add(InterceptorsWrapper(
        onRequest: (options, handler) {
          options.headers['Authorization'] = 'Bearer $userToken';
          return handler.next(options);
        },
      ));
    }
  }
}
