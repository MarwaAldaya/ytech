class AppUrls {
  static const String baseUrl = "https://asalback3.ytechcloud.com/api";
  static const String storageUrl = "https://asalback3.ytechcloud.com/storage";

  // users
  static const String loginUrl = "$baseUrl/MobileTokenAuth/LoginTerminal";
  static const String baseCustomerUrl = "$baseUrl/services/app/MobileCustomer";

  static const String createCustomerUrl = "$baseCustomerUrl/CreateNewCustomer";
  static const String getFilteredCustomersUrl =
      "$baseCustomerUrl/GetFilteredCustomers";
}
