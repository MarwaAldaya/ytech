// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'result_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResultResponse<T> _$ResultResponseFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    ResultResponse<T>(
      code: json['code'] as int,
      data: _$nullableGenericFromJson(json['data'], fromJsonT),
      result: json['result'] as bool,
      message: json['message'] as String,
      totalRecords: json['totalRecords'] as int,
      imgsUrl: json['imgsUrl'] as String?,
    );

Map<String, dynamic> _$ResultResponseToJson<T>(
  ResultResponse<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'code': instance.code,
      'data': _$nullableGenericToJson(instance.data, toJsonT),
      'result': instance.result,
      'message': instance.message,
      'totalRecords': instance.totalRecords,
      'imgsUrl': instance.imgsUrl,
    };

T? _$nullableGenericFromJson<T>(
  Object? input,
  T Function(Object? json) fromJson,
) =>
    input == null ? null : fromJson(input);

Object? _$nullableGenericToJson<T>(
  T? input,
  Object? Function(T value) toJson,
) =>
    input == null ? null : toJson(input);
