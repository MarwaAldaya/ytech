import 'package:json_annotation/json_annotation.dart';

part 'result_response.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ResultResponse<T> {
  int code;
  final T? data;
  bool result;
  String message;
  int totalRecords;
  String? imgsUrl;

  ResultResponse({
    required this.code,
    required this.data,
    required this.result,
    required this.message,
    required this.totalRecords,
    this.imgsUrl,
  });

  factory ResultResponse.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$ResultResponseFromJson(json, fromJsonT);
}
