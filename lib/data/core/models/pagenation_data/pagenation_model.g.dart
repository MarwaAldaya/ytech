// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pagenation_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pagination<T> _$PaginationFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    Pagination<T>(
      total: json['total'] as int,
      data: (json['data'] as List<dynamic>).map(fromJsonT).toList(),
      lastPage: json['last_page'] as int,
      currentPage: json['current_page'] as int,
    );

Map<String, dynamic> _$PaginationToJson<T>(
  Pagination<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'data': instance.data.map(toJsonT).toList(),
      'total': instance.total,
      'current_page': instance.currentPage,
      'last_page': instance.lastPage,
    };
