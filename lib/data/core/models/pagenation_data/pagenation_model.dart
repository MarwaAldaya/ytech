import 'package:json_annotation/json_annotation.dart';

part 'pagenation_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)

class Pagination<T> {
  final List<T> data;
  int total;
  @JsonKey(name: 'current_page')
  int currentPage;
  //int limit;
  @JsonKey(name: "last_page")
  int lastPage;


  Pagination({
    required this.total,
    required this.data,
    required this.lastPage,
    required this.currentPage,
   // required this.limit
  });

  factory Pagination.fromJson(
      Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$PaginationFromJson(json, fromJsonT);
}
