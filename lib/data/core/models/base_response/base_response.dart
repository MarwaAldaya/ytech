import 'package:json_annotation/json_annotation.dart';

import '../result_response/result_response.dart';

part 'base_response.g.dart';

//flutter pub get && flutter pub run build_runner build --delete-conflicting-outputs
@JsonSerializable(genericArgumentFactories: true)
class BaseResponse<T> {
  ResultResponse<T>? result;
  dynamic targetUrl;
  bool success;
  dynamic error;
  bool unAuthorizedRequest;
  @JsonKey(name: "__abp")
  bool abp;

  BaseResponse({
    this.result,
    this.targetUrl,
    required this.success,
    this.error,
    required this.unAuthorizedRequest,
    required this.abp,
  });



  factory BaseResponse.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$BaseResponseFromJson(json, fromJsonT);
}
