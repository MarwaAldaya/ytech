import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';

import '../../../app/app_prefrences.dart';
import '../../../app/dependency_injection.dart';

import '../models/base_response/base_response.dart';
import 'app_api_helper.dart';

class AppApiHelperImpl implements AppApiHelper {
  Dio get dio => _dio;

  final Dio _dio;
  String? userToken;

  AppApiHelperImpl(this._dio);

  getToken() async {
    await instance<AppPreferences>()
        .getAccessToken()
        .then((value) => userToken = value);

    ///we add this for presentation.presentation.login route because there is no token yet
    if (userToken!.isNotEmpty) {
      _dio.options.headers.addAll({"Authorization": "Bearer ${userToken!}"});
    }
  }

  @override
  Future<BaseResponse<T>> performGetRequest<T>(
      String endpoint, T Function(Map<String, dynamic>) fromJson,
      {Map<String, dynamic>? queryParameters}) async {
    await getToken();
    final response = await dio.get(endpoint,
        queryParameters: queryParameters,
        options: Options(
          headers: _dio.options.headers,
        ));
    // return fromJson(response.data! as Map<String, dynamic>);
    final baseResponse = BaseResponse<T>.fromJson(
        response.data!, (json) => fromJson(json as Map<String, dynamic>));

    fromJson(json.decode(response.data)['result']);
    log("GetRequest ${baseResponse.toString()}");

    return baseResponse;
  }

  @override
  Future<BaseResponse<T>> performPostRequest<T>(
      String endpoint,
      Map<String, dynamic>? data,
      T Function(Map<String, dynamic>) fromJson,{bool injectToken = true}) async {
    if(injectToken)await getToken();
    final response = await dio.post(endpoint,
        data: data,
        options: Options(
          headers: _dio.options.headers,
        ));
    final baseResponse = BaseResponse<T>.fromJson(
        response.data!, (json) => fromJson(json as Map<String, dynamic>));

    return baseResponse;
  }

  @override
  Future<BaseResponse<T>> performGetListRequest<T>(
      String endpoint, T Function(List p1) fromJson,{Map<String, dynamic>? queryParameters}) async {
    await getToken();
    final response = await dio.get(endpoint,
        queryParameters: queryParameters,
        options: Options(
          headers: _dio.options.headers,
        ));
    final baseResponse = BaseResponse<T>.fromJson(response.data!,
        (json) => fromJson(response.data!['result']['data'] as List<dynamic>));

    return baseResponse;
  }
}
