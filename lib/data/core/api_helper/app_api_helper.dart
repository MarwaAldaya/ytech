import '../models/base_response/base_response.dart';

abstract class AppApiHelper {
  ///[injectToken] in login route we wont need token in header so we pass this parameter [false] value
  Future<BaseResponse<T>> performPostRequest<T>(String endpoint,
      Map<String, dynamic> data, T Function(Map<String, dynamic>) fromJson,
      {bool injectToken = true});

  Future<BaseResponse<T>> performGetListRequest<T>(
      String endpoint, T Function(List<dynamic>) fromJson,
      {Map<String, dynamic>? queryParameters});

  Future<BaseResponse<T>> performGetRequest<T>(
      String endpoint, T Function(Map<String, dynamic>) fromJson,
      {Map<String, dynamic>? queryParameters});
}
