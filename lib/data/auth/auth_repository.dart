import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:ytech/domain/parameters/auth_parameter/login_params.dart';

import '../../app/app_prefrences.dart';
import '../../app/dependency_injection.dart';
import '../../domain/core/entities/failure.dart';
import '../core/utils/error_handler/error_handler.dart';
import '../core/utils/network/base_network_info.dart';
import 'auth_remote_data_source.dart';

abstract class BaseAuthRepository {
  Future<Either<Failure, bool>>login(LoginParams loginParams);
}

class AuthRepositoryImpl extends BaseAuthRepository {
  final BaseAuthRemoteDataSource _authRemoteDataSource;
  final NetworkInfo _networkInfo;

  AuthRepositoryImpl(this._authRemoteDataSource, this._networkInfo);

  @override
  Future<Either<Failure, bool>>login(LoginParams loginParams) async {
    if (await _networkInfo.isConnected) {
      // its connected to internet, its safe to call API
      try {
        final response = await _authRemoteDataSource.login(loginParams);
// print(response.result!.result);
        if (response.result != null) {
          if (response.result!.result ) {
            // success
            instance<AppPreferences>()
                .setAccessToken(response.result!.data!.accessToken);
            return const Right(true);
          } else {
            // failure --return business error
            return Left(Failure(ApiInternalStatus.FAILURE ? 1 : 0,
                response.result?.message ?? ResponseMessage.DEFAULT));
          }
        } else {
          // failure --return business error
          return Left(Failure(ApiInternalStatus.FAILURE ? 1 : 0,
              response.error ?? ResponseMessage.DEFAULT));
        }
    }
    catch (error) {
    log(error.toString());
    return Left(ErrorHandler.handle(error).failure);
    }
    } else {
      // return internet connection error
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
