import 'package:ytech/domain/parameters/auth_parameter/login_params.dart';

import '../core/api_helper/app_api_helper.dart';
import '../core/models/base_response/base_response.dart';
import '../core/utils/api_routes/api_routes.dart';
import 'auth_model.dart';

abstract class BaseAuthRemoteDataSource {
  Future<BaseResponse<AuthModel>> login(LoginParams loginParams);
}

class AuthRemoteDataSourceImpl implements BaseAuthRemoteDataSource {
  final AppApiHelper _appApiHelper;

  AuthRemoteDataSourceImpl(this._appApiHelper);

  @override
  Future<BaseResponse<AuthModel>> login(LoginParams loginParams) async {
    return await _appApiHelper.performPostRequest(
        AppUrls.loginUrl, loginParams.toJson(), AuthModel.fromJson,injectToken: false);
  }
}
