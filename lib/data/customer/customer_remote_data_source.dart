import 'package:ytech/data/customer/customer_model.dart';
import 'package:ytech/domain/parameters/customer_parameter/create_customer_params.dart';

import '../../domain/core/pagination_params/pagination_params.dart';
import '../core/api_helper/app_api_helper.dart';
import '../core/models/base_response/base_response.dart';
import '../core/utils/api_routes/api_routes.dart';

abstract class BaseCustomerRemoteDataSource {
  Future<BaseResponse<CustomerModel>> createCustomer(
      CreateCustomerParams createCustomerParams);

  Future<BaseResponse<CustomersModel>> getFilteredCustomers(
      PaginationParams paginationParams);
}

class CustomerRemoteDataSourceImpl implements BaseCustomerRemoteDataSource {
  final AppApiHelper _appApiHelper;

  CustomerRemoteDataSourceImpl(this._appApiHelper);

  @override
  Future<BaseResponse<CustomerModel>> createCustomer(
      CreateCustomerParams createCustomerParams) async {
    return await _appApiHelper.performPostRequest(AppUrls.createCustomerUrl,
        createCustomerParams.toJson(), CustomerModel.fromJson);
  }

  @override
  Future<BaseResponse<CustomersModel>> getFilteredCustomers(
      PaginationParams paginationParams) async {
    final params=paginationParams.toJson();
    params.removeWhere((key, value) => value==null);
    return await _appApiHelper.performGetListRequest(
      AppUrls.getFilteredCustomersUrl,
      CustomersModel.fromJson,
      queryParameters: params
    );
  }
}
