import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:ytech/data/customer/customer_mapper.dart';
import 'package:ytech/data/customer/customer_remote_data_source.dart';
import 'package:ytech/domain/core/pagination_params/pagination_params.dart';
import 'package:ytech/domain/entities/customer_entities/customer_entity.dart';
import 'package:ytech/domain/parameters/customer_parameter/create_customer_params.dart';

import '../../domain/core/entities/failure.dart';
import '../core/utils/error_handler/error_handler.dart';
import '../core/utils/network/base_network_info.dart';

abstract class BaseCustomerRepository {
  Future<Either<Failure, CustomerEntity>> createCustomer(
      CreateCustomerParams createCustomerParams);

  Future<Either<Failure, List<CustomerEntity>>> getCustomers(
      PaginationParams paginationParams);
}

class CustomerRepositoryImpl extends BaseCustomerRepository {
  final BaseCustomerRemoteDataSource _customerRemoteDataSource;
  final NetworkInfo _networkInfo;

  CustomerRepositoryImpl(this._customerRemoteDataSource, this._networkInfo);

  @override
  Future<Either<Failure, CustomerEntity>> createCustomer(
      CreateCustomerParams createCustomerParams) async {
    if (await _networkInfo.isConnected) {
      // its connected to internet, its safe to call API
     // try {
        final response = await _customerRemoteDataSource
            .createCustomer(createCustomerParams);

        if (response.result != null) {
          if (response.result!.result) {
            // success
            return Right(response.result!.data!.toDomain());
          } else {
            // failure --return business error
            return Left(Failure(ApiInternalStatus.FAILURE ? 1 : 0,
                response.result?.message ?? ResponseMessage.DEFAULT));
          }
        } else {
          // failure --return business error
          return Left(Failure(ApiInternalStatus.FAILURE ? 1 : 0,
              response.error ?? ResponseMessage.DEFAULT));
        }
      // } catch (error) {
      //   log(error.toString());
      //   return Left(ErrorHandler.handle(error).failure);
      // }
    } else {
      // return internet connection error
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }

  @override
  Future<Either<Failure, List<CustomerEntity>>> getCustomers(
      PaginationParams paginationParams) async {
    if (await _networkInfo.isConnected) {
      // its connected to internet, its safe to call API
      //try {
        final response = await _customerRemoteDataSource
            .getFilteredCustomers(paginationParams);

        if (response.result != null) {
          if (response.result!.result) {
            // success
            return Right(response.result!.data!.toDomain());
          } else {
            // failure --return business error
            return Left(Failure(ApiInternalStatus.FAILURE ? 1 : 0,
                response.result?.message ?? ResponseMessage.DEFAULT));
          }
        } else {
          // failure --return business error
          return Left(Failure(ApiInternalStatus.FAILURE ? 1 : 0,
              response.error ?? ResponseMessage.DEFAULT));
        }
      // } catch (error) {
      //   log(error.toString());
      //   return Left(ErrorHandler.handle(error).failure);
      // }
    } else {
      // return internet connection error
      return Left(DataSource.NO_INTERNET_CONNECTION.getFailure());
    }
  }
}
