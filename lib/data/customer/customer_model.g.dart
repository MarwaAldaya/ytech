// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerModel _$CustomerModelFromJson(Map<String, dynamic> json) =>
    CustomerModel(
      id: json['id'] as String,
      fullNameEn: json['enFullName'] as String,
      fullNameAr: json['arFullName'] as String,
      displayName: json['displayName'] as String,
      address: json['address'] as String?,
      email: json['email'] as String?,
      commercialRegister: json['commercialRegister'] as String?,
      phoneNumber: json['phoneNumber'] as String,
      city: json['city'] as String?,
      store: json['store'] as String?,
      taxIdNumber: json['taxIdNumber'] as String?,
      loyalty: json['loyalty'] as bool?,
      isDelivery: json['isDelivery'] as bool?,
      commission: (json['commission'] as num?)?.toDouble(),
      loyaltyBalance: (json['loyaltyBalance'] as num?)?.toDouble(),
      isActive: json['isActive'] as bool?,
      isDefault: json['isDefault'] as bool?,
      customerCategoryId: json['customerCategoryId'] as String?,
      creditLimit: (json['creditLimit'] as num?)?.toDouble(),
      financialAccount: json['financialAccount'] as String?,
      creationTime: DateTime.parse(json['creationTime'] as String),
    );

Map<String, dynamic> _$CustomerModelToJson(CustomerModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'enFullName': instance.fullNameEn,
      'arFullName': instance.fullNameAr,
      'displayName': instance.displayName,
      'address': instance.address,
      'email': instance.email,
      'commercialRegister': instance.commercialRegister,
      'phoneNumber': instance.phoneNumber,
      'city': instance.city,
      'store': instance.store,
      'taxIdNumber': instance.taxIdNumber,
      'loyalty': instance.loyalty,
      'isDelivery': instance.isDelivery,
      'commission': instance.commission,
      'loyaltyBalance': instance.loyaltyBalance,
      'isActive': instance.isActive,
      'isDefault': instance.isDefault,
      'customerCategoryId': instance.customerCategoryId,
      'creditLimit': instance.creditLimit,
      'financialAccount': instance.financialAccount,
      'creationTime': instance.creationTime.toIso8601String(),
    };
