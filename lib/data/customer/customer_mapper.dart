import 'package:ytech/data/customer/customer_model.dart';
import 'package:ytech/domain/entities/customer_entities/customer_entity.dart';

extension CustomersModelMapper on CustomersModel {
  List<CustomerEntity> toDomain() {
    List<CustomerEntity> all = [];
    customers?.forEach((element) {
      all.add(element.toDomain());
    });
    return all;
  }
}

extension CustomerModelMapper on CustomerModel {
  CustomerEntity toDomain() {
    return CustomerEntity(id: id,
        fullNameEn: fullNameEn,
        fullNameAr: fullNameAr,
        displayName: displayName,
        address: address,
        email: email,
        commercialRegister: commercialRegister,
        phoneNumber: phoneNumber,
        city: city,
        store: store,
        taxIdNumber: taxIdNumber,
        loyalty: loyalty,
        isDelivery: isDelivery,
        commission: commission,
        loyaltyBalance: loyaltyBalance,
        isActive: isActive,
        isDefault: isDefault,
        customerCategoryId: customerCategoryId,
        creditLimit: creditLimit,
        financialAccount: financialAccount,
        creationTime: creationTime);
  }
}
