// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ytech/presentation/login/login_binding.dart';

import '../rescources/routes_manager.dart';
import '../rescources/theme_manager.dart';

class MyApp extends StatelessWidget {
  MyApp._internal(); // private named constructor
  int appState = 0;
  static final MyApp instance =
      MyApp._internal(); // single instance -- singleton

  factory MyApp() => instance; // factory for the class instance

  bool light = true;

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(430, 930),
        minTextAdapt: true,
        splitScreenMode: false,
        builder: (BuildContext context, Widget? child) {
          appController.height = 1.sh;
          appController.width = 1.sw;
          return GetMaterialApp(
            getPages: AppPages.pages,
            debugShowCheckedModeBanner: false,
            initialBinding: LoginBinding(),
            initialRoute: Routes.loginRoute,
            theme: getLightTheme(),
          );
        });
  }
}
