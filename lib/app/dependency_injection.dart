import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'package:get_storage/get_storage.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:ytech/data/auth/auth_remote_data_source.dart';
import 'package:ytech/data/auth/auth_repository.dart';
import 'package:ytech/data/customer/customer_remote_data_source.dart';
import 'package:ytech/data/customer/customer_repository.dart';
import 'package:ytech/presentation/widget/drawer/main_drawer_controller.dart';

import '../data/core/api_helper/app_api_helper.dart';
import '../data/core/api_helper/app_api_helper_Impl.dart';
import '../data/core/utils/dio_factory.dart';
import '../data/core/utils/network/base_network_info.dart';
import '../data/core/utils/network/network_info.dart';
import '../presentation/main_page/main_page_controller.dart';
import 'app_controller.dart';
import 'app_prefrences.dart';

final instance = GetIt.instance;

///
/// The [init] function is responsible for adding the instances to the graph
///
Future<void> init() async {
  //! External

  /// Adding the [GetStorage] instance to the graph to be later used by [AppPreferences]
  final storagePrefs = GetStorage();

  instance.registerLazySingleton<GetStorage>(() => storagePrefs);

  /// Adding the [AppPreferences] instance to the graph to be later used by the local data sources
  instance
      .registerLazySingleton<AppPreferences>(() => AppPreferences(instance()));

  /// network info
  instance.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(InternetConnectionChecker()));

  ///  DioFactory
  instance.registerLazySingleton<DioFactory>(() => DioFactory(instance()));

  /// Data sources
  Dio dio = await instance<DioFactory>().getDio();
  instance.registerLazySingleton<AppApiHelper>(() => AppApiHelperImpl(dio));

  Get.put(AppController(), permanent: true);

  Get.put(MainDrawerController(), permanent: true);

  Get.put(MainPageController(), permanent: true);


}

initAuthModule() {
  if (!instance.isRegistered<BaseAuthRemoteDataSource>()) {
    instance.registerFactory<BaseAuthRemoteDataSource>(
        () => AuthRemoteDataSourceImpl(instance<AppApiHelper>()));
  }
  if (!instance.isRegistered<BaseAuthRepository>()) {
    instance.registerFactory<BaseAuthRepository>(
      () => AuthRepositoryImpl(
        instance(),
        instance(),
      ),
    );
  }
}
initCustomerModule() {
  if (!instance.isRegistered<BaseCustomerRemoteDataSource>()) {
    instance.registerFactory<BaseCustomerRemoteDataSource>(
        () => CustomerRemoteDataSourceImpl(instance<AppApiHelper>()));
  }
  if (!instance.isRegistered<BaseCustomerRepository>()) {
    instance.registerFactory<BaseCustomerRepository>(
      () => CustomerRepositoryImpl(
        instance(),
        instance(),
      ),
    );
  }
}


