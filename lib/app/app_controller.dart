import 'package:get/get.dart';

class AppController extends GetxController {
  final RxDouble _height = 1.0.obs;
  final RxDouble _width = 1.0.obs;
  double get height => _height.value;

  set height(double value) {
    _height.value = value;
  }

  double get width => _width.value;

  set width(double value) {
    _width.value = value;
  }
}
