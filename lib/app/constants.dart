//flutter pub run build_runner build
class Constants {
  static const String empty = "";
  static const int zero = 0;
  static const double zeroD = 0.0;
  static const int apiTimeOut = 60000;
}
