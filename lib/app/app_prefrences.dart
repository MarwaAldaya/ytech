import 'package:get_storage/get_storage.dart';

class AppPreferences {
  final GetStorage _storagePreferences;

  AppPreferences(this._storagePreferences);

  final String accessTokenKey = "ACCESS_TOKEN_KEY";

  //access token
  Future<void> setAccessToken(String accessToken) async {
    _storagePreferences.write(accessTokenKey, accessToken);
  }

  Future<String> getAccessToken() async {
    return _storagePreferences.read(accessTokenKey) ?? "";
  }

  Future<void> removeAccessToken() async {
    _storagePreferences.remove(accessTokenKey);
  }
}
