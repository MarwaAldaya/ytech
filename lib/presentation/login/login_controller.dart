import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:ytech/app/dependency_injection.dart';
import 'package:ytech/app/extensions.dart';
import 'package:ytech/data/auth/auth_repository.dart';
import 'package:ytech/domain/parameters/auth_parameter/login_params.dart';
import 'package:ytech/presentation/widget/toasts_messages.dart';
import 'package:ytech/rescources/routes_manager.dart';

class LoginController extends GetxController {
  TextEditingController userNameOrMobileNumber =
      TextEditingController(text: 'user1');
  TextEditingController password = TextEditingController(text: "12345678");
  TextEditingController terminalKey = TextEditingController(
      text: "GNrHw7jLaV5AFdi20m5HI02fVTn+hCBkJPr7UL7ThSw=");
  final RxBool isUserNameOrMobileValid = true.obs;
  final RxBool isPasswordValid = true.obs;
  final RxBool isTerminalKeyValid = true.obs;
  final RxBool isPasswordVisible = false.obs;
  final RxBool isLoading = false.obs;

  void _checkUserNameOrMobileValid() {
    isUserNameOrMobileValid.value = userNameOrMobileNumber.text.isNotEmpty;
  }

  void _checkPasswordValid() {
    isPasswordValid.value = password.text.isNotEmpty;
  }

  void _checkTerminalKeyValid() {
    isTerminalKeyValid.value = terminalKey.text.isNotEmpty;
  }

  bool _checkAllValid() {
    _checkUserNameOrMobileValid();
    _checkPasswordValid();
    _checkTerminalKeyValid();
    return isTerminalKeyValid.value &&
        isPasswordValid.value &&
        isUserNameOrMobileValid.value;
  }

  void login() async {
    if (_checkAllValid()) {
      BaseAuthRepository baseAuthRepository = instance();
      isLoading.value = true;
      (await baseAuthRepository.login(LoginParams(
              username: userNameOrMobileNumber.text, password: password.text)))
          .fold(
              (failure) => {
                    log(failure.message.orEmpty()),
                    showFlutterToast(message: failure.message.orEmpty()),
                  },

              (result) => Get.offNamed(Routes.customersRoute));
      isLoading.value = false;
    }
  }

  @override
  void onInit() {
    super.onInit();
    userNameOrMobileNumber.addListener(() {
      _checkUserNameOrMobileValid();
    });
    password.addListener(() {
      _checkPasswordValid();
    });
    terminalKey.addListener(() {
      _checkTerminalKeyValid();
    });
  }
}
