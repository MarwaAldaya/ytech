import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ytech/presentation/login/login_controller.dart';
import 'package:ytech/presentation/widget/custom_elevated_button.dart';
import 'package:ytech/presentation/widget/custom_text_field_title.dart';
import 'package:ytech/presentation/widget/custome_text_widget.dart';
import 'package:ytech/rescources/assets_manager.dart';
import 'package:ytech/rescources/strings_manager.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Get.theme.scaffoldBackgroundColor,
      body: Row(
        children: [
          Expanded(
            flex: 3,
            child: SingleChildScrollView(
              child: Padding(
                padding:  EdgeInsets.symmetric(horizontal: 0.07.sw),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          top: 0.1.sh, right: 0.05.sw, left: 0.05.sw),
                      child: CustomTextWidget(
                        text:
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore ",
                        style: Get.theme.textTheme.bodySmall
                            ?.copyWith(fontSize: 18.sp),
                        maxLines: 2,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    62.verticalSpace,
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 0.01.sw),
                      child: Column(
                        children: [
                          CustomTextFieldTitle(
                            text: AppStrings.userName,
                            icon: Icons.person_2_outlined,
                          ),
                          12.verticalSpace,
                          SizedBox(
                            // height: 0.05.sh,
                            child: Obx(
                              () => TextField(
                                controller: controller.userNameOrMobileNumber,
                                decoration: InputDecoration(
                                  errorText:
                                      controller.isUserNameOrMobileValid.value
                                          ? null
                                          : AppStrings.invalidInput,
                                  filled: true,
                                  fillColor: Get.theme.colorScheme.onSecondary
                                      .withOpacity(0.3),
                                ),
                              ),
                            ),
                          ),
                          22.verticalSpace,
                          CustomTextFieldTitle(
                            text: AppStrings.password,
                            icon: Icons.lock_open,
                          ),
                          12.verticalSpace,
                          SizedBox(
                            // height: 0.05.sh,
                            child: Obx(
                              () => TextField(
                                controller: controller.password,
                                obscureText: !controller.isPasswordVisible.value,
                                decoration: InputDecoration(
                                  errorText: controller.isPasswordValid.value
                                      ? null
                                      : AppStrings.invalidPassword,
                                  filled: true,
                                  fillColor: Get.theme.colorScheme.onSecondary
                                      .withOpacity(0.3),
                                  suffixIcon: InkWell(
                                    borderRadius: BorderRadius.circular(32.r),
                                    onTap: () {
                                      controller.isPasswordVisible.value =
                                          !controller.isPasswordVisible.value;
                                    },
                                    child: Icon(
                                      controller.isPasswordVisible.value
                                          ? Icons.visibility_outlined
                                          : Icons.visibility_off_outlined,
                                      color: Get.theme.colorScheme.onSecondary,
                                      size: 25.sp,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          22.verticalSpace,
                          CustomTextFieldTitle(
                            text: AppStrings.terminalKey,
                            customIcon: Transform.rotate(
                              angle: pi / 2,
                              child: Icon(
                                Icons.key,
                                color: Get.theme.colorScheme.background,
                                size: 0.015.sw,
                              ),
                            ),
                          ),
                          12.verticalSpace,
                          SizedBox(
                            // height: 0.05.sh,
                            child: Obx(
                              () => TextField(
                                readOnly: true,
                                controller: controller.terminalKey,
                                decoration: InputDecoration(
                                    errorText: controller.isTerminalKeyValid.value
                                        ? null
                                        : AppStrings.invalidTerminalKey,
                                    filled: true,
                                    fillColor: Get.theme.colorScheme.onSecondary
                                        .withOpacity(0.3)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    62.verticalSpace,
                    Obx(() => controller.isLoading.value
                        ? CircularProgressIndicator(
                            color: Get.theme.colorScheme.secondary,
                          )
                        : CustomElevatedButton(
                            title: AppStrings.login,
                            onTap: () => controller.login()))
                  ],
                ),
              ),
            ),
          ),
          Expanded(
              flex: 4,
              child: Image.asset(
                ImageAssets.login,
                // scale: 1.5,
              ))
        ],
      ),
    );
  }
}
