import 'package:get/get.dart';
import 'package:ytech/app/dependency_injection.dart';

import 'login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    initAuthModule();
    Get.put(LoginController());
  }
}
