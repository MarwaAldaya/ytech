import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:ytech/app/extensions.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../app/dependency_injection.dart';
import '../../data/customer/customer_repository.dart';
import '../../domain/core/pagination_params/pagination_params.dart';
import '../../domain/entities/customer_entities/customer_entity.dart';
import '../../rescources/color_manager.dart';
import '../../rescources/strings_manager.dart';
import '../widget/toasts_messages.dart';

class CustomersController extends GetxController {
  TextEditingController search = TextEditingController();
  RefreshController refreshController = RefreshController();
  RxString category = AppStrings.category.obs;
  final RxBool isLoading = false.obs;
  int currentPage = 1;

  Map<String, Color> statusColor = {
    "Active": ColorManager.green,
    "Inactive": ColorManager.pink,
    "Delivery": ColorManager.yellow,
  };

  RxList<CustomerEntity> customers = <CustomerEntity>[].obs;

  String getStatus(customer) {
    return customer.isDelivery!
        ? "Delivery"
        : customer.isActive!
            ? "Active"
            : "inactive";
  }

  getAllCustomers({required int page}) async {
    BaseCustomerRepository baseCustomerRepository = instance();

    (await baseCustomerRepository
            .getCustomers(PaginationParams(pageNumber: page)))
        .fold(
            (failure) => {
                  log(failure.message.orEmpty()),
                  showFlutterToast(message: failure.message.orEmpty()),
                }, (List<CustomerEntity> customers) {
      return currentPage == 1
          ? this.customers.value = customers
          : this.customers.value.addAll(customers);
    });

    isLoading.value = false;
  }

  onLoading() async {
    isLoading.value = true;
    await getAllCustomers(page: currentPage++);

    // isLoading.value = false;
    refreshController.loadComplete();
  }

  onRefresh() async {
    isLoading.value = true;
    await getAllCustomers(page: 1);

    refreshController.refreshCompleted();
  }

  addNewCustomerToListLocally(CustomerEntity newCustomer) {
    customers.add(newCustomer);
    customers.refresh();
  }

  @override
  void onInit() async {
    isLoading.value = true;
    await getAllCustomers(page: 1);
    super.onInit();
  }
}
