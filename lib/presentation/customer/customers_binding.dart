import 'package:get/get.dart';

import '../../app/dependency_injection.dart';
import 'customers_controller.dart';

class CustomersBinding extends Bindings {
  @override
  void dependencies() {
    initCustomerModule();
    Get.put(CustomersController());
  }
}
