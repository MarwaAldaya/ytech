import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import 'package:ytech/rescources/assets_manager.dart';
import 'package:ytech/rescources/color_manager.dart';

class CustomerLoadingCard extends StatelessWidget {

  const CustomerLoadingCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Container(
            height: 50.h,
            alignment: Alignment.centerLeft,
            child: Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Image.asset(ImageAssets.person),
            ),
          ),
        ),

        Expanded(
          flex: 3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildShimmer(height: 0.015.sh, width: 0.06.sw),
              12.verticalSpace,
              buildShimmer(height: 0.015.sh, width: 0.04.sw),
            ],
          ),
        ),
        Expanded(
          flex: 5,
          child: Column(
            children: [
              buildShimmer(height: 0.015.sh, width: 0.06.sw),
              12.verticalSpace,
              buildShimmer(height: 0.015.sh, width: 0.06.sw),
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child:   buildShimmer(height: 0.06.sh, width: 0.04.sw),
        ),
        Expanded(
          flex: 1,
          child: Icon(
            Icons.edit,
            color: ColorManager.grey,
            size: 7.w,
          ),
        ),
        20.horizontalSpace,
      ],
    );
  }
  Shimmer buildShimmer({
    required double width,
    required double height,
  }) {
    return Shimmer.fromColors(
      direction: ShimmerDirection.ltr,
      baseColor: ColorManager.grey,
      highlightColor: Get.theme.cardColor,
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
            color: ColorManager.grey.withOpacity(0.4),
            borderRadius: BorderRadius.circular(15.r)),
      ),
    );
  }
}
