import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../rescources/color_manager.dart';
import '../../../rescources/strings_manager.dart';
import '../../widget/empty_page.dart';
import '../customers_controller.dart';
import 'customer_card.dart';
import 'customer_card_loading.dart';

class CustomerBody extends GetView<CustomersController> {
  const CustomerBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SizedBox(
        height: 0.8.sh,
        child: SmartRefresher(
          controller: controller.refreshController,
          enablePullDown: true,
          enablePullUp: true,
          onRefresh: () async => await controller.onRefresh(),
          onLoading: () async => await controller.onLoading(),
          footer: CustomFooter(
            builder: (BuildContext context, LoadStatus? mode) {
              Widget body;
              if (mode == LoadStatus.loading) {
                body = const CupertinoActivityIndicator();
              } else if (mode == LoadStatus.failed) {
                body = Text(AppStrings().failClickToRefresh);
              } else if (mode == LoadStatus.canLoading) {
                body = Text(AppStrings().pullUpToLoadMore);
              } else {
                body = Text(AppStrings().noMoreData);
              }
              return SizedBox(
                height: 55.h,
                child: Center(child: body),
              );
            },
          ),
          child: ListView.separated(
            itemCount:
                controller.isLoading.value ? 7 : controller.customers.length,
            itemBuilder: (BuildContext context, int index) {
              return controller.isLoading.value
                  ? const CustomerLoadingCard()
                  : controller.customers.isNotEmpty
                      ? CustomerCard(
                          customer: controller.customers.value[index])
                      : const EmptyPage();
            },
            separatorBuilder: (BuildContext context, int index) => Divider(
              height: 20.h,
              indent: 25.w,
              endIndent: 25.w,
              thickness: 0.3.h,
              color: ColorManager.lightGrey,
            ),
          ),
        ),
      ),
    );
  }
}
