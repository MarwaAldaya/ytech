import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ytech/app/extensions.dart';
import 'package:ytech/domain/entities/customer_entities/customer_entity.dart';
import 'package:ytech/presentation/widget/custome_text_widget.dart';
import 'package:ytech/rescources/assets_manager.dart';
import 'package:ytech/rescources/color_manager.dart';

import '../../../rescources/strings_manager.dart';
import '../customers_controller.dart';

class CustomerCard extends GetView<CustomersController> {
  final CustomerEntity customer;

  const CustomerCard({Key? key, required this.customer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Container(
            height: 50.h,
            alignment: Alignment.centerLeft,
            //color: ColorManager.grey,
            child: Image.asset(ImageAssets.person),
          ),
        ),
        Expanded(
          flex: 3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomTextWidget(text: customer.fullNameEn),
              Row(
                children: [
                  Icon(
                    Icons.check_circle,
                    size: 5.w,
                    color:
                        controller.statusColor[controller.getStatus(customer)],
                  ),
                  2.horizontalSpace,
                  Expanded(
                    child: CustomTextWidget(
                      text: controller.getStatus(customer),
                      style: Get.textTheme.bodyMedium!.copyWith(
                          color: controller
                              .statusColor[controller.getStatus(customer)],
                          fontSize: 15.sp),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Expanded(
          flex: 5,
          child: Column(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.phone,
                    color: ColorManager.grey,
                    size: 5.w,
                  ),
                  2.horizontalSpace,
                  Expanded(
                    child: CustomTextWidget(
                      text: customer.phoneNumber,
                      style:
                          Get.textTheme.bodyMedium!.copyWith(fontSize: 15.sp),
                    ),
                  ),
                ],
              ),
              4.verticalSpace,
              Row(
                children: [
                  Icon(
                    Icons.contacts,
                    color: ColorManager.grey,
                    size: 5.w,
                  ),
                  2.horizontalSpace,
                  Expanded(
                    child: CustomTextWidget(
                      text: customer.address.orEmpty(),
                      style:
                          Get.textTheme.bodyMedium!.copyWith(fontSize: 15.sp),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.grey.shade50,
                borderRadius: BorderRadius.circular(8.r)),
            child: Padding(
              padding: EdgeInsets.all(2.w),
              child: Column(
                children: [
                  CustomTextWidget(
                    text: "Balance ${customer.loyaltyBalance ?? "-"} SAR",
                    style: Get.textTheme.bodyMedium!.copyWith(fontSize: 15.sp),
                  ),
                  CustomTextWidget(
                    text: "Balance ${customer.commission} points",
                    style: Get.textTheme.bodyMedium!.copyWith(fontSize: 15.sp),
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Icon(
            Icons.edit,
            color: ColorManager.grey,
            size: 7.w,
          ),
        ),
        20.horizontalSpace,
      ],
    );
  }
}
