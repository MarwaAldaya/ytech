import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../app/app_controller.dart';
import '../../../rescources/color_manager.dart';
import '../../../rescources/routes_manager.dart';
import '../../../rescources/strings_manager.dart';
import '../../widget/custome_text_widget.dart';
import '../customers_controller.dart';

class CustomerHeader extends GetView<CustomersController> {
  const CustomerHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: Get.find<AppController>().width * 0.34,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: CustomTextWidget(
                        text: AppStrings.customers,
                        style: Get.theme.textTheme.titleLarge
                            ?.copyWith(fontSize: 9.w),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          alignment: Alignment.center,
                          height: 40.h,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12.r),
                            border: Border.all(
                                width: 0.5, color: ColorManager.green),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Icon(
                                  Icons.circle,
                                  color: ColorManager.green,
                                  size: 3.w,
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Text(
                                  AppStrings.active,
                                  style: Get.textTheme.displayMedium!.copyWith(
                                    fontSize: 4.w,
                                    color: ColorManager.green,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          alignment: Alignment.center,
                          height: 40.h,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12.r),
                            border: Border.all(
                                width: 0.5, color: ColorManager.pink),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Icon(
                                  Icons.circle,
                                  color: ColorManager.pink,
                                  size: 3.w,
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Text(
                                  AppStrings.inactive,
                                  style: Get.textTheme.displayMedium!.copyWith(
                                    fontSize: 4.w,
                                    color: ColorManager.pink,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          alignment: Alignment.center,
                          height: 40.h,
                          decoration: BoxDecoration(
                            color: ColorManager.yellow,
                            borderRadius: BorderRadius.circular(12.r),
                            //border: Border.all(width: 0.5, color: ColorManager.pink),
                          ),
                          child: Text(
                            AppStrings.deliveryBig,
                            style: Get.textTheme.displayMedium!
                                .copyWith(fontSize: 4.w),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 50.h,
                child: TextField(
                  controller: controller.search,
                  decoration: InputDecoration(
                      hintText: AppStrings.searchByCustomerName,
                      hintStyle: Get.theme.inputDecorationTheme.hintStyle!
                          .copyWith(fontSize: 17.sp),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(13.r)),
                        borderSide: BorderSide(color: ColorManager.lightGrey),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(13.r)),
                        borderSide: BorderSide(color: ColorManager.lightGrey),
                      ),
                      prefixIcon: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 4.w),
                        child: Icon(
                          Icons.search,
                          size: 8.w,
                        ),
                      )),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: Get.width * 0.3,
          child: Row(
            children: [
              Expanded(
                flex: 2,
                child: DropdownSearch<String>(
                  items: const ["category1", "category2"],
                  popupProps: const PopupPropsMultiSelection.menu(
                    showSelectedItems: true,
                    showSearchBox: true,
                  ),
                  selectedItem: controller.category.value,
                  onChanged: (String? data) =>
                  controller.category.value = data ?? "",
                  dropdownDecoratorProps: DropDownDecoratorProps(
                    textAlignVertical: TextAlignVertical.center,
                    dropdownSearchDecoration: InputDecoration(
                      // prefixText: controller.category.value.isEmpty
                      //     ? ""
                      //     : AppStrings.category,
                      // prefixStyle: Get.theme.inputDecorationTheme.hintStyle!
                      //     .copyWith(
                      //     fontSize: 20.sp,
                      //     color: ColorManager.primaryColor),
                      contentPadding: EdgeInsets.symmetric(horizontal: 2.w),
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Padding(
                  padding: EdgeInsets.only(right: 18.w),
                  child: InkWell(
                    onTap: () => Get.offNamed(Routes.createCustomerRoute),
                    child: Container(
                      height: 42.h,
                      padding:
                      EdgeInsets.symmetric(vertical: 6.h, horizontal: 6.w),
                      decoration: BoxDecoration(
                        color: ColorManager.primaryColor,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.r),
                          topRight: Radius.circular(20.r),
                          bottomRight: Radius.circular(20.r),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.add,
                            color: ColorManager.white,
                            size: 5.w,
                          ),
                          2.horizontalSpace,
                          Text(
                            AppStrings.addCustomer,
                            style: Get.textTheme.displayMedium!.copyWith(
                              fontSize: 16.sp,
                              color: ColorManager.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
