import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../main_page/main_page_view.dart';
import 'customers_controller.dart';
import 'widgets/customer_body.dart';
import 'widgets/customer_header.dart';

class CustomersView extends GetView<CustomersController> {
  const CustomersView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MainPageView(
        child: buildCustomer(),
      ),
    );
  }

  Widget buildCustomer() {
    return SizedBox(
      height: 1.sh,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const CustomerHeader(),
            20.verticalSpace,
            const CustomerBody(),
          ],
        ),
      ),
    );
  }
}
