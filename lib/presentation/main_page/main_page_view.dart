import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ytech/app/app_controller.dart';
import 'package:ytech/presentation/widget/custome_text_widget.dart';
import 'package:ytech/rescources/strings_manager.dart';

import '../widget/drawer/main_drawer.dart';

class MainPageView extends GetView<AppController> {
  final Widget child;
  final Widget? title;

  const MainPageView({required this.child, this.title, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(
            width: 75.5,
            child: Drawer(
              elevation: 0,
              backgroundColor: Colors.white,
              child: MainDrawer(),
            ),
          ),
          Obx(
            () => Expanded(
              // flex: 3,
              child: Padding(
                padding: EdgeInsets.only(left: controller.width * 0.03),
                child: SingleChildScrollView(
                  child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if (title != null)
                        Padding(
                          padding:
                              EdgeInsets.only(left: controller.width * 0.023),
                          child: title,
                        ),
                      if (title != null) 72.verticalSpace,
                      child,
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
