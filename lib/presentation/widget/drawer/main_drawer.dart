import 'package:flutter/material.dart';
import 'package:curved_drawer_fork/curved_drawer_fork.dart';
import 'package:get/get.dart';
import 'package:ytech/rescources/routes_manager.dart';

import '../../../rescources/color_manager.dart';
import 'main_drawer_controller.dart';

class MainDrawer extends GetWidget<MainDrawerController> {
  const MainDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => CurvedDrawer(
        index: controller.index.value,
        color: ColorManager.black1,
        labelColor: ColorManager.white,
        width: 75.5,
        items: controller.drawerItems,
        onTap: (newIndex) {
         // Get.offAllNamed(Routes.customersRoute);
          controller.index.value = newIndex;
        },
      ),
    );
  }
}
