import 'package:curved_drawer_fork/curved_drawer_fork.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainDrawerController extends GetxController {
  final List<DrawerItem> drawerItems = <DrawerItem>[
    const DrawerItem(icon: Icon(Icons.menu)),
    const DrawerItem(icon: Icon(Icons.calculate_outlined), label: "Invoices"),
    const DrawerItem(icon: Icon(Icons.sell_outlined), label: "Selling"),
    const DrawerItem(icon: Icon(Icons.production_quantity_limits), label: "Products"),
    const DrawerItem(icon: Icon(Icons.people), label: "Customers"),
    const DrawerItem(icon: Icon(Icons.sticky_note_2),label: "Reports"),
    const DrawerItem(icon: Icon(Icons.settings), label: "Settings"),
  ];
  RxInt index = 4.obs;
}
