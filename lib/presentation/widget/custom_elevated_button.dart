import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class CustomElevatedButton extends StatelessWidget {
  final double? height;
  final double? width;
  final String title;
  final TextStyle? textStyle;
  final TextAlign? textAlign;
  final Function onTap;
  final double? borderRadius;

  const CustomElevatedButton(
      {required this.title,
      Key? key,
      this.height,
      this.width,
      this.textStyle,
      this.textAlign,
      required this.onTap,
      this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? 60.w,
      height: height ?? 65.h,
      child: ElevatedButton(
          onPressed: () {
            onTap();
          },
          style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(borderRadius ?? 100.r))),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  title,
                  textAlign: textAlign ?? TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: textStyle ??
                      Get.theme.textTheme.displayMedium
                          ?.copyWith(fontSize: 23.sp),
                ),
              )
            ],
          )),
    );
  }
}
