import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../rescources/assets_manager.dart';
import '../../rescources/strings_manager.dart';

class EmptyPage extends StatelessWidget {
  const EmptyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 2,
      child: Center(
        child: Column(
          children: [
            SizedBox(height: MediaQuery.of(context).size.height / 15),
            Container(
                width: MediaQuery.of(context).size.width / 2,
                padding: const EdgeInsets.only(left: 10),
                child: Image.asset(ImageAssets.noData)),
            SizedBox(height: MediaQuery.of(context).size.height / 30),
            Text(
              AppStrings().screenIsEmpty,
              style: Get.textTheme.labelLarge!.copyWith(fontSize: 23.sp),
            ),
          ],
        ),
      ),
    );
  }
}
