import 'package:flutter/material.dart';
import 'dart:math' as math;

import '../../rescources/value_manager.dart';

class CustomIcon extends StatelessWidget {
  final Function onPressed;
  final Widget? icon;
  final Color? iconColor;
  final Color? backGroundColor;
  final double? rightPadding;
  final double? leftPadding;
  final double? width;
  final double? height;

  const CustomIcon(
      {required this.onPressed,
      this.icon,
      Key? key,
      this.rightPadding,
      this.leftPadding,
      this.iconColor,
      this.backGroundColor,
      this.height,
      this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Card(
      elevation: 2,
      color: backGroundColor ?? theme.cardColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: InkWell(
        onTap: () {
          onPressed();
        },
        borderRadius: BorderRadius.circular(10),
        child: SizedBox(
          width: width ?? AppSize.s40,
          height: height ?? AppSize.s40,
          child: Padding(
            padding: EdgeInsets.only(
                left: leftPadding != null ? leftPadding! : AppPadding.p4,
                right: rightPadding != null ? rightPadding! : AppPadding.p4),
            child: icon,
          ),
        ),
      ),
    );
  }
}
