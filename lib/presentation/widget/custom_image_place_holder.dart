import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomImagePlaceHolder extends StatelessWidget {
  final Color? backgroundColor;
  final double? width;
  final double? height;
  final double? radius;
  final Widget? child;

  const CustomImagePlaceHolder(
      {Key? key,
      this.backgroundColor,
      this.width,
      this.height,
      this.radius,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: backgroundColor ?? Colors.grey[300],
            borderRadius: BorderRadius.circular(radius ?? 14.r)),
        width: width ?? 1.sw,
        height: height ?? 200.h,
        child: child );
  }
}
