import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ytech/presentation/widget/custome_text_widget.dart';

class CustomTextFieldTitle extends StatelessWidget {
  final String text;
  final IconData? icon;
  final Widget? customIcon;

  const CustomTextFieldTitle(
      {Key? key, required this.text, this.icon, this.customIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
        child: customIcon ??
            Icon(
              icon,
              color: Get.theme.colorScheme.background,
              size: 0.015.sw,
            ),
      ),
      Expanded(
        flex: 8,
        child: CustomTextWidget(
          text: text,
          style: Get.theme.textTheme.bodyMedium?.copyWith(fontSize: 20.sp),
          // maxLines: 1,
          // textAlign: TextAlign.center,
        ),
      ),
    ]);
  }
}
