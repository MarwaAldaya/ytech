import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ytech/presentation/create_customer/create_customer_controller.dart';
import 'package:ytech/presentation/main_page/main_page_view.dart';
import 'package:ytech/presentation/widget/custom_elevated_button.dart';
import 'package:ytech/presentation/widget/custome_text_widget.dart';
import 'package:ytech/rescources/assets_manager.dart';
import 'package:ytech/rescources/design_constants.dart';
import 'package:ytech/rescources/routes_manager.dart';
import 'package:ytech/rescources/strings_manager.dart';

class CreateCustomerView extends GetView<CreateCustomerController> {
  const CreateCustomerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Get.theme.scaffoldBackgroundColor,
        body: MainPageView(
          title: CustomTextWidget(
            text: AppStrings.customers,
            style: Get.theme.textTheme.titleLarge?.copyWith(fontSize: 52.sp),
          ),
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 0.07.sw),
                  child: Column(
                    children: [
                      Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: Obx(
                                  () => TextField(
                                    controller: controller.nameAr,
                                    decoration: InputDecoration(
                                      hintText: AppStrings.customerArabicName,
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 2.w),
                                      errorText: controller.isNameArValid.value
                                          ? null
                                          : AppStrings.invalidCustomerName,
                                      enabledBorder: kUnderlinedBorderGrey,
                                      focusedBorder: kUnderlinedBorderPrimary,
                                      focusedErrorBorder:
                                          kUnderlinedBorderError,
                                      errorBorder: kUnderlinedBorderError,
                                    ),
                                  ),
                                ),
                              ),
                              const Spacer(),
                              Expanded(
                                flex: 4,
                                child: Obx(
                                  () => TextField(
                                    controller: controller.nameEn,
                                    decoration: InputDecoration(
                                      hintText: AppStrings.customerEnName,
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 2.w),
                                      errorText: controller.isNameEnValid.value
                                          ? null
                                          : AppStrings.invalidCustomerName,
                                      enabledBorder: kUnderlinedBorderGrey,
                                      focusedBorder: kUnderlinedBorderPrimary,
                                      focusedErrorBorder:
                                          kUnderlinedBorderError,
                                      errorBorder: kUnderlinedBorderError,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          12.verticalSpace,
                          Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: TextField(
                                  controller: controller.address,
                                  decoration: InputDecoration(
                                    hintText: AppStrings.address,
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 2.w),
                                    enabledBorder: kUnderlinedBorderGrey,
                                    focusedBorder: kUnderlinedBorderPrimary,
                                    focusedErrorBorder: kUnderlinedBorderError,
                                    errorBorder: kUnderlinedBorderError,
                                  ),
                                ),
                              ),
                              const Spacer(),
                              Expanded(
                                flex: 4,
                                child: Obx(
                                  () => DropdownSearch<String>(
                                    items: controller.cities,
                                    itemAsString: (String u) => u,
                                    popupProps:
                                        const PopupPropsMultiSelection.menu(
                                      showSelectedItems: true,
                                      showSearchBox: true,
                                    ),
                                    // enabled: controller.canEdit.value,
                                    selectedItem: controller.city.value,
                                    onChanged: (String? data) =>
                                        controller.city.value = data ?? "",
                                    dropdownDecoratorProps:
                                        DropDownDecoratorProps(
                                      textAlignVertical:
                                          TextAlignVertical.center,
                                      dropdownSearchDecoration: InputDecoration(
                                        prefixText:
                                            controller.city.value.isNotEmpty
                                                ? ""
                                                : AppStrings.city,
                                        prefixStyle: Get.theme
                                            .inputDecorationTheme.hintStyle,
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 2.w),
                                        errorText: controller.isCityValid.value
                                            ? null
                                            : AppStrings.invalidCity,
                                        enabledBorder: kUnderlinedBorderGrey,
                                        focusedBorder: kUnderlinedBorderPrimary,
                                        focusedErrorBorder:
                                            kUnderlinedBorderError,
                                        errorBorder: kUnderlinedBorderError,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          12.verticalSpace,
                          Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: Obx(
                                  () => TextField(
                                    controller: controller.phoneNumber,
                                    keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.allow(
                                          RegExp(r'[0-9]')),
                                    ],
                                    decoration: InputDecoration(
                                      hintText: AppStrings.phoneNumber,
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 2.w),
                                      errorText:
                                          controller.isPhoneNumberValid.value
                                              ? null
                                              : AppStrings.invalidPhoneNumber,
                                      enabledBorder: kUnderlinedBorderGrey,
                                      focusedBorder: kUnderlinedBorderPrimary,
                                      focusedErrorBorder:
                                          kUnderlinedBorderError,
                                      errorBorder: kUnderlinedBorderError,
                                    ),
                                  ),
                                ),
                              ),
                              const Spacer(),
                              Expanded(
                                flex: 4,
                                child: TextField(
                                  controller: controller.email,
                                  decoration: InputDecoration(
                                    hintText: AppStrings.email,
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 2.w),
                                    enabledBorder: kUnderlinedBorderGrey,
                                    focusedBorder: kUnderlinedBorderPrimary,
                                    focusedErrorBorder: kUnderlinedBorderError,
                                    errorBorder: kUnderlinedBorderError,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          12.verticalSpace,
                          Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: TextField(
                                  controller: controller.commercialRegister,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(
                                        RegExp(r'[0-9]')),
                                  ],
                                  decoration: InputDecoration(
                                    hintText: AppStrings.commercialRegister,
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 2.w),
                                    enabledBorder: kUnderlinedBorderGrey,
                                    focusedBorder: kUnderlinedBorderPrimary,
                                    focusedErrorBorder: kUnderlinedBorderError,
                                    errorBorder: kUnderlinedBorderError,
                                  ),
                                ),
                              ),
                              const Spacer(),
                              Expanded(
                                flex: 4,
                                child: TextField(
                                  controller: controller.taxNumber,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(
                                        RegExp(r'[0-9]')),
                                  ],
                                  decoration: InputDecoration(
                                    hintText: AppStrings.taxNumber,
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 2.w),
                                    enabledBorder: kUnderlinedBorderGrey,
                                    focusedBorder: kUnderlinedBorderPrimary,
                                    focusedErrorBorder: kUnderlinedBorderError,
                                    errorBorder: kUnderlinedBorderError,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          12.verticalSpace,
                          Row(
                            children: [
                              Expanded(
                                flex: 4,
                                child: Obx(() => DropdownSearch<String>(
                                      items: controller.stores,
                                      itemAsString: (String u) => u,
                                      popupProps:
                                          const PopupPropsMultiSelection.menu(
                                        showSelectedItems: true,
                                        showSearchBox: true,
                                      ),
                                      selectedItem: controller.firstStore.value,
                                      onChanged: (String? data) => controller
                                          .firstStore.value = data ?? "",
                                      dropdownDecoratorProps:
                                          DropDownDecoratorProps(
                                        textAlignVertical:
                                            TextAlignVertical.center,
                                        dropdownSearchDecoration:
                                            InputDecoration(
                                          prefixText: controller
                                                  .firstStore.value.isNotEmpty
                                              ? ""
                                              : AppStrings.firstStore,
                                          prefixStyle: Get.theme
                                              .inputDecorationTheme.hintStyle,
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal: 2.w),
                                          errorText: controller
                                                  .isFirstStoreValid.value
                                              ? null
                                              : AppStrings.invalidFirstStore,
                                          enabledBorder: kUnderlinedBorderGrey,
                                          focusedBorder:
                                              kUnderlinedBorderPrimary,
                                          focusedErrorBorder:
                                              kUnderlinedBorderError,
                                          errorBorder: kUnderlinedBorderError,
                                        ),
                                      ),
                                    )),
                              ),
                              const Spacer(),
                              Expanded(
                                flex: 4,
                                child: Obx(() => DropdownSearch<String>(
                                      items: controller.customersCategories,
                                      itemAsString: (String u) => u,
                                      popupProps:
                                          const PopupPropsMultiSelection.menu(
                                        showSelectedItems: true,
                                        showSearchBox: true,
                                      ),
                                      selectedItem:
                                          controller.customerCategoryName.value,
                                      onChanged: (String? data) => controller
                                          .customerCategoryName
                                          .value = data ?? "",
                                      dropdownDecoratorProps:
                                          DropDownDecoratorProps(
                                        textAlignVertical:
                                            TextAlignVertical.center,
                                        dropdownSearchDecoration:
                                            InputDecoration(
                                          prefixText: controller
                                                  .customerCategoryName
                                                  .value
                                                  .isNotEmpty
                                              ? ""
                                              : AppStrings.customerCategoryName,
                                          prefixStyle: Get.theme
                                              .inputDecorationTheme.hintStyle,
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal: 2.w),
                                          enabledBorder: kUnderlinedBorderGrey,
                                          focusedBorder:
                                              kUnderlinedBorderPrimary,
                                          focusedErrorBorder:
                                              kUnderlinedBorderError,
                                          errorBorder: kUnderlinedBorderError,
                                        ),
                                      ),
                                    )),
                              ),
                            ],
                          ),
                          32.verticalSpace,
                          Row(
                            children: [
                              Container(
                                width: 20.sp,
                                height: 20.sp,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4.r),
                                  border: Border.all(
                                    color: Get.theme.primaryColorLight,
                                    // set the border color here
                                    width: 1.0, // set the border width here
                                  ),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(1.7.sp),
                                  child: Obx(
                                    () => InkWell(
                                      borderRadius: BorderRadius.circular(2.r),
                                      onTap: () {
                                        controller.isActive.value =
                                            !controller.isActive.value;
                                      },
                                      child: Container(
                                        width: 20.sp,
                                        height: 20.sp,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(2.r),
                                          color: controller.isActive.value
                                              ? Get.theme.primaryColorLight
                                              : null,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              4.horizontalSpace,
                              Text(
                                AppStrings.active,
                                style: Get.theme.textTheme.labelMedium,
                              )
                            ],
                          ),
                          42.verticalSpace,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomElevatedButton(
                                  title: AppStrings.back,
                                  width: 40.w,
                                  borderRadius: 12.r,
                                  onTap: () =>
                                      Get.offNamed(Routes.customersRoute)),
                              Obx(
                                () => controller.isLoading.value
                                    ? CircularProgressIndicator(
                                        color: Get.theme.colorScheme.secondary,
                                      )
                                    : CustomElevatedButton(
                                        title: AppStrings.save,
                                        width: 40.w,
                                        borderRadius: 12.r,
                                        onTap: () => controller.save()),
                              ),
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                  flex: 3,
                  child: Image.asset(
                    ImageAssets.card,
                    scale: 1.7,
                  ))
            ],
          ),
        ));
  }
}
