import 'package:get/get.dart';
import 'package:ytech/app/dependency_injection.dart';
import 'package:ytech/presentation/create_customer/create_customer_controller.dart';

class CreateCustomerBinding extends Bindings {
  @override
  void dependencies() {
    initCustomerModule();
    Get.put(CreateCustomerController());
  }
}
