import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';
import 'package:ytech/app/dependency_injection.dart';
import 'package:ytech/app/extensions.dart';
import 'package:ytech/data/customer/customer_repository.dart';
import 'package:ytech/domain/entities/customer_entities/customer_entity.dart';
import 'package:ytech/domain/parameters/customer_parameter/create_customer_params.dart';
import 'package:ytech/presentation/customer/customers_controller.dart';
import 'package:ytech/presentation/widget/toasts_messages.dart';

class CreateCustomerController extends GetxController {
  TextEditingController nameAr = TextEditingController();
  TextEditingController nameEn = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController phoneNumber = TextEditingController();
  TextEditingController commercialRegister = TextEditingController();
  TextEditingController taxNumber = TextEditingController();
  TextEditingController address = TextEditingController();
  final Rx<String> city = "".obs;
  final Rx<String> firstStore = "".obs;
  final Rx<String> customerCategoryName = "".obs;

  ///this lists will get it from api
  ///but we fix it just here
  List<String> cities = ["Syria", "Palestine"];
  List<String> stores = ["store1", "store2"];
  List<String> customersCategories = ["customer1", "customer2"];

  final RxBool isNameArValid = true.obs;
  final RxBool isNameEnValid = true.obs;
  final RxBool isPhoneNumberValid = true.obs;
  final RxBool isCityValid = true.obs;
  final RxBool isFirstStoreValid = true.obs;
  final RxBool isActive = false.obs;

  final RxBool isLoading = false.obs;

  bool _isEmailValid(String email) {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
  }

  void _checkNameArValid() {
    isNameArValid.value = nameAr.text.isNotEmpty;
  }

  void _checkNameEnValid() {
    isNameEnValid.value = nameEn.text.isNotEmpty;
  }

  void _checkPhoneNumberValid() {
    isPhoneNumberValid.value = phoneNumber.text.isNotEmpty;
  }

  void _checkCityValid() {
    isCityValid.value = city.isNotEmpty;
  }

  void _checkFirstStoreValid() {
    isFirstStoreValid.value = firstStore.isNotEmpty;
  }

  bool _checkAllValid() {
    _checkNameArValid();
    _checkNameEnValid();
    _checkPhoneNumberValid();
    _checkCityValid();
    _checkFirstStoreValid();
    return isNameArValid.value &&
        isNameEnValid.value &&
        isPhoneNumberValid.value &&
        isCityValid.value &&
        isFirstStoreValid.value; // &&
  }

  save() async {
    if (_checkAllValid()) {
      BaseCustomerRepository baseCustomerRepository = instance();
      isLoading.value = true;
      String id = generateRandomUuid();
      (await baseCustomerRepository.createCustomer(CreateCustomerParams(
              id: id,
              fullNameEn: nameEn.text,
              fullNameAr: nameAr.text,
              address: address.text.isEmpty ? null : address.text,
              email: email.text.isEmpty ? null : email.text,
              commercialRegister: commercialRegister.text.isEmpty
                  ? null
                  : commercialRegister.text,
              phoneNumber: phoneNumber.text,
              taxIdNumber: taxNumber.text.isEmpty ? null : taxNumber.text,
              isDelivery: false,
              //customerCategoryId: customerCategoryId,
              isActive: isActive.value)))
          .fold(
              (failure) => {
                    log(failure.message.orEmpty()),
                    showFlutterToast(message: failure.message.orEmpty()),
                  },
              (customer) =>
                  {addCreatedCustomerToCustomersLocally(customer), Get.back()});

      isLoading.value = false;
    }
  }

  /// to not request get customers again from api
  addCreatedCustomerToCustomersLocally(CustomerEntity createdCustomer) {
    if (Get.isRegistered<CustomersController>()) {
      Get.find<CustomersController>()
          .addNewCustomerToListLocally(createdCustomer);
    }
  }

  String generateRandomUuid() {
    var uid = const Uuid();
    String uuid = uid.v4();
    return uuid;
  }

  @override
  void onInit() {
    super.onInit();
    nameAr.addListener(() {
      _checkNameArValid();
    });
    nameEn.addListener(() {
      _checkNameEnValid();
    });
  }
}
