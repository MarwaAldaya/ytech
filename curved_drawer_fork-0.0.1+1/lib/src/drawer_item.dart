part of '../curved_drawer_fork.dart';

/// The drawer item with label and icon.
class DrawerItem {
  /// The label of this item.
  final String label;

  /// The icon of this item.
  final Icon icon;

  /// Creates an instance of [DrawerItem]
  const DrawerItem({
    this.label = "",
    required this.icon,
  });
}
