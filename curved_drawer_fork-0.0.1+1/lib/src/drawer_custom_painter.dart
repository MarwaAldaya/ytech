part of '../curved_drawer_fork.dart';

/// The curved drawer painter
class _DrawerCustomPainter extends CustomPainter {
  late double _loc;
  late double _s;

  /// The color of the drawer
  Color color;

  /// The direction of the drawer
  TextDirection textDirection;

  /// The width of the drawer
  double width;

  /// Weather the drawer is end drawer
  bool isEndDrawer;

  _DrawerCustomPainter(double startingLoc, int itemsLength, this.color,
      this.textDirection, this.width, this.isEndDrawer) {
    //Determine % for each itemz
    final span = 1.0 / itemsLength;
    //standard deviation from location
    _s = width / 100 * .1;
    double l = startingLoc + (span - _s) / 2;
    _loc = l;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;

    final leftPath = Path()
      ..moveTo(width, 0)
      ..lineTo(width, (_loc - 0.02) * size.height)
      ..cubicTo(
        width * 0.90,
        (_loc + _s * 0.2) * size.height,
        width * 0.50,
        _loc * size.height,
        width * 0.500,
        (_loc + _s * 0.50) * size.height,
      )
      ..cubicTo(
        width * 0.50,
        (_loc + _s) * size.height,
        width,
        (_loc + _s - _s * 0.10) * size.height,
        width,
        (_loc + _s + 0.02) * size.height,
      )
      ..lineTo(width, size.height)
      ..lineTo(0, size.height)
      ..lineTo(0, 0)
      ..close();
    final rightPath = Path()
      ..moveTo(size.width - width, 0)
      ..lineTo(size.width - width, (_loc - 0.02) * size.height)
      ..cubicTo(
        size.width - width * 0.90,
        (_loc + _s * 0.2) * size.height,
        size.width - (width * 0.50),
        _loc * size.height,
        size.width - (width * 0.500),
        (_loc + _s * 0.50) * size.height,
      )
      ..cubicTo(
        size.width - (width * 0.50),
        (_loc + _s) * size.height,
        size.width - width,
        (_loc + _s - _s * 0.10) * size.height,
        size.width - width,
        (_loc + _s + 0.02) * size.height,
      )
      ..lineTo(size.width - width, size.height)
      ..lineTo(size.width, size.height)
      ..lineTo(size.width, 0)
      ..close();
    final path = isEndDrawer ? rightPath : leftPath;
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return this != oldDelegate;
  }
}
