part of '../curved_drawer_fork.dart';

/// Drawer navigation item.
class DrawerNavItem extends StatelessWidget {
  /// The icon of this item.
  final Icon icon;

  /// The label text of this item.
  final String label;

  /// The size of this item.
  final double size;

  /// The color of this item.
  final Color color;

  /// The background color of this item.
  final Color background;

  /// Creates an instance of [DrawerNavItem]
  const DrawerNavItem(
      {Key? key,
      required this.icon,
      this.label = "",
      this.size = 25.0,
      this.color = Colors.black54,
      this.background = Colors.white})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(size)),
      color: background,
      type: MaterialType.button,
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Container(
          padding: const EdgeInsets.all(12),
          decoration: BoxDecoration(
            color: ColorManager.yellow,
            borderRadius: BorderRadius.circular(100),
          ),
          child: Icon(
            icon.icon,
            size: size,
            color: color,
          ),
        ),
      ),
    );
  }
}

class NavButton extends StatelessWidget {
  final double position;
  final int length;
  final int index;
  final bool isEndDrawer;
  final double? width;
  final Color? color;
  final ValueChanged<int> onTap;
  final Icon icon;
  final String text;

  NavButton(
      {required this.onTap,
      required this.position,
      required this.length,
      required this.isEndDrawer,
      this.width,
      this.color,
      required this.text,
      required this.index,
      required this.icon});

  @override
  Widget build(BuildContext context) {
    final desiredPosition = 1.0 / length * index;
    final difference = (position - desiredPosition).abs();
    final verticalAlignment = 1 - length * difference;
    final opacity = length * difference;
    final directionMultiplier = isEndDrawer ? 1 : -1;
    return Expanded(
      child: GestureDetector(
        onTap: () {
          onTap(index);
        },
        child: SizedBox(
            width: width,
            child: Transform.translate(
              offset: Offset(
                  difference < 1.0 / length
                      ? verticalAlignment * directionMultiplier * 40
                      : 0,
                  0),
              child: Opacity(
                  opacity: difference < 1.0 / length * 0.99 ? opacity : 1.0,
                  child: Column(
                    children: [
                      Expanded(
                        child: Icon(
                          icon.icon,
                          color: color,
                        ),
                      ),
                      Expanded(
                        child: Text(
                          text,
                          style: TextStyle(fontSize: 16.sp),
                        ),
                      )
                    ],
                  )),
            )),
      ),
    );
  }
}
