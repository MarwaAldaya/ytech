library curved_drawer_fork;

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ytech/rescources/color_manager.dart';

part 'src/drawer_item.dart';
part 'src/drawer_nav_item.dart';
part 'src/drawer_custom_painter.dart';
part 'src/curved_drawer.dart';
